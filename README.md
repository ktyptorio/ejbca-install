# Instalasi EJBCA CE 7.4.3.2 pada Wildfly 10.1.0 di CentOS 7

Spesifikasi server (dijalankan pada Virtual Machine)
* RAM = 6 GB
* CPUs = 6
* Hard disk = 50 GB

System Prerequisites
* EJBCA CE 7.4.3.2
* OS = CentOS 7
* Application Server = Wildfly-10.1.0.Final
* Database = MariaDB
* Java = OpenJDK 8
* Build Tool = Apache Ant

## 1. Konfigurasi Awal CentOS 7

`sudo yum update`<br/>
`sudo yum install vim ntp ntpdate epel-release wget tar unzip java-1.8.0.openjdk-devel git ant psmisc mariadb bc patch`<br/>
`sudo yum update install`<br/>

## 2. Konfigurasi Firewall

`sudo firewall-cmd --zone=public --permanent --add-port=80/tcp`<br/>
`sudo firewall-cmd --zone=public --permanent --add-port=443/tcp`<br/>
`sudo firewall-cmd --zone=public --permanent --add-port=8443/tcp`<br/>
`sudo firewall-cmd --zone=public --permanent --add-port=9990/tcp`<br/>
`sudo firewall-cmd --zone=public --permanent --add-port=8080/tcp`<br/>
`sudo firewall-cmd --zone=public --permanent --add-port=8442/tcp`<br/>
`sudo firewall-cmd --zone=public --permanent --add-port=4447/tcp`<br/>

## 3. Instalasi dan Konfigurasi Database MariaDB
### Instalasi Database MariaDB

`sudo yum install mariadb-server`<br/>
`sudo systemctl enable mariadb`<br/>
`sudo systemctl start mariadb`<br/>
`sudo systemctl status mariadb`<br/>
`sudo mysql_secure_installation`<br/>

### Konfigurasi Database MariaDB

`mysql -u root -p`<br/>
`mysql> CREATE DATABASE ejbca CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;`<br/>
`mysql> GRANT ALL PRIVILEGES ON ejbca.* TO 'ejbca'@'localhost' IDENTIFIED BY 'ejbca';`<br/>

## 4. Instalasi Application Server Wildfly-10.1.0.Final
### Instalasi Wildfly

Masuk ke directory `/tmp`

`wget https://download.jboss.org/wildfly/10.1.0.Final/wildfly-10.1.0.Final.tar.gz`

Membuat user dan group sistem wildfly

`sudo groupadd -r wildfly`<br/>
`sudo useradd -r -g wildfly -d /opt/wildfly -s /sbin/nologin wildfly`<br/>
`sudo tar -zxf wildfly-10.1.0.Final.tar.gz -C /opt/`<br/>
`sudo ln -s /opt/wildfly-10.1.0.Final /opt/wildfly`<br/>

Masuk ke directory `/opt`

`sudo chown -RH wildfly: /opt/wildfly`<br/>
`sudo mkdir -p /etc/wildfly`<br/>
`sudo cp /opt/wildfly/docs/contrib/scripts/systemd/wildfly.conf /etc/wildfly/`<br/>
`sudo cp /opt/wildfly/docs/contrib/scripts/systemd/launch.sh /opt/wildfly/bin/`<br/>
`sudo sh -c 'chmod +x /opt/wildfly/bin/*.sh'`<br/>
`sudo cp /opt/wildfly/docs/contrib/scripts/systemd/wildfly.service /etc/systemd/system/`<br/>
`vi /opt/wildfly/bin/standalone.conf`<br/>

Masukkan script `  JAVA_OPTS="-Xms2048m -Xmx2048m -Djava.net.preferIPv4Stack=true"` kedalam standalone.conf

`sudo systemctl daemon-reload`<br/>
`sudo systemctl enable wildfly`<br/>
`sudo systemctl start wildfly`<br/>
`reboot`<br/>

Akses `localhost:8080`, wildfly berhasil diinstall jika dapat mengakses halaman wildfly

### Konfigurasi Datasource

Untuk melakukan konfigurasi datasource, masuk ke direktori `/tmp`

`wget https://downloads.mariadb.com/Connectors/java/connector-java-2.2.6/mariadb-java-client-2.2.6.jar`<br/>
`sudo -u wildfly cp /tmp/mariadb-java-client-2.2.6.jar /opt/wildfly/standalone/deployments/mariadb-java-client.jar`<br/>
`cd /opt/wildfly/bin`<br/>
`./jboss-cli.sh -c`<br/>

Menambahkan datasource untuk database ejbca pada saat terkoneksi ke `standalone@localhost:9990`

`data-source add --name=ejbcads --driver-name="mariadb-java-client.jar" --connection-url="jdbc:mysql://127.0.0.1:3306/ejbca" --jndi-name="java:/EjbcaDS" --use-ccm=true --driver-class="org.mariadb.jdbc.Driver" --user-name="ejbca" --password="ejbca" --validate-on-match=true --background-validation=false --prepared-statements-cache-size=50 --share-prepared-statements=true --min-pool-size=5 --max-pool-size=150 --pool-prefill=true --transaction-isolation=TRANSACTION_READ_COMMITTED --check-valid-connection-sql="select 1;"`<br/>
`:reload`<br/>
`quit`<br/>

Menambahkan datasource untuk konfigurasi WildFly Remoting

`/subsystem=remoting/http-connector=http-remoting-connector:remove`<br/>
`/subsystem=remoting/http-connector=http-remoting-connector:add(connector-ref="remoting",security-realm="ApplicationRealm")`<br/>
`/socket-binding-group=standard-sockets/socket-binding=remoting:add(port="4447")`<br/>
`/subsystem=undertow/server=default-server/http-listener=remoting:add(socket-binding=remoting)`<br/>
`:reload`<br/>

Menambahkan datasource untuk logging

`/subsystem=logging/logger=org.ejbca:add`<br/>
`/subsystem=logging/logger=org.ejbca:write-attribute(name=level, value=DEBUG)`<br/>
`/subsystem=logging/logger=org.cesecore:add`<br/>
`/subsystem=logging/logger=org.cesecore:write-attribute(name=level, value=DEBUG)`<br/>

Menambahkan datasource untuk konfigurasi TLS

`/subsystem=undertow/server=default-server/http-listener=default:remove`<br/>
`/subsystem=undertow/server=default-server/https-listener=https:remove`<br/>
`/socket-binding-group=standard-sockets/socket-binding=http:remove`<br/>
`/socket-binding-group=standard-sockets/socket-binding=https:remove`<br/>
`:reload`<br/>

`/interface=http:add(inet-address="0.0.0.0")`<br/>
`/interface=httpspub:add(inet-address="0.0.0.0")`<br/>
`/interface=httpspriv:add(inet-address="0.0.0.0")`<br/>
`/socket-binding-group=standard-sockets/socket-binding=http:add(port="8080",interface="http")`<br/>
`/subsystem=undertow/server=default-server/http-listener=http:add(socket-binding=http)`<br/>
`/subsystem=undertow/server=default-server/http-listener=http:write-attribute(name=redirect-socket, value="httpspriv")`<br/>
`:reload`<br/>

`/core-service=management/security-realm=SSLRealm:add()`<br/>
`/core-service=management/security-realm=SSLRealm/server-identity=ssl:add(keystore-path="${jboss.server.config.dir}/keystore/keystore.jks", keystore-password="serverpwd", alias="localhost")`<br/>
`/core-service=management/security-realm=SSLRealm/authentication=truststore:add(keystore-path="${jboss.server.config.dir}/keystore/truststore.jks", keystore-password="changeit")`<br/>
`/socket-binding-group=standard-sockets/socket-binding=httpspriv:add(port="8443",interface="httpspriv")`<br/>
`/socket-binding-group=standard-sockets/socket-binding=httpspub:add(port="8442", interface="httpspub")`<br/>

`/subsystem=undertow/server=default-server/https-listener=httpspriv:add(socket-binding=httpspriv, security-realm="SSLRealm", verify-client=REQUIRED)`<br/>
`/subsystem=undertow/server=default-server/https-listener=httpspriv:write-attribute(name=max-parameters, value="2048")`<br/>
`/subsystem=undertow/server=default-server/https-listener=httpspub:add(socket-binding=httpspub, security-realm="SSLRealm")`<br/>
`/subsystem=undertow/server=default-server/https-listener=httpspub:write-attribute(name=max-parameters, value="2048")`<br/>
`:reload`<br/>

`/system-property=org.apache.tomcat.util.buf.UDecoder.ALLOW_ENCODED_SLASH:add(value=true)`<br/>
`/system-property=org.apache.catalina.connector.CoyoteAdapter.ALLOW_BACKSLASH:add(value=true)`<br/>
`/system-property=org.apache.catalina.connector.URI_ENCODING:add(value="UTF-8")`<br/>
`/system-property=org.apache.catalina.connector.USE_BODY_ENCODING_FOR_QUERY_STRING:add(value=true)`<br/>
`/subsystem=webservices:write-attribute(name=wsdl-host, value=jbossws.undefined.host)`<br/>
`/subsystem=webservices:write-attribute(name=modify-wsdl-address, value=true)`<br/>
`:reload`<br/>

## 5. Instalasi EJBCA

Masuk ke directory `/tmp`

`wget https://sourceforge.net/projects/ejbca/files/ejbca7/ejbca_ce_7_4_3_2.zip`<br/>
`mv ejbca_ce_7_4_3_2.zip /opt`<br/>

Masuk ke directory `/opt`

`sudo unzip ejbca_ce_7_4_3_2.zip`

### Konfigurasi Properties EJBCA

`cd ejbca_ce_7_4_3_2/conf/`<br/>
`sudo cp database.properties.sample database.properties`<br/>
`sudo cp cesecore.properties.sample cesecore.properties`<br/>
`sudo cp ejbca.properties.sample ejbca.properties`<br/>
`sudo cp install.properties.sample install.properties`<br/>
`sudo cp web.properties.sample web.properties`<br/>

#### a. Konfigurasi properties database EJBCA

`vi database.properties`

Setting properties database berikut ini:

`database.name=mysql`<br/>
`database.url=jdbc:mysql://127.0.0.1:3306/ejbca?characterEncoding=UTF-8`<br/>
`database.driver=org.mariadb.jdbc.Driver`<br/>
`database.username=ejbca`<br/>
`database.password=ejbca`<br/>

#### b. Konfigurasi properties EJBCA

`vi ejbca.properties`

Setting properties ejbca berikut ini:

`appserver.home=/opt/wildfly-10.1.0.Final`<br/>

#### c. Konfigurasi properties install EJBCA

`vi install.properties`

Setting properties install berikut ini:

`ca.name=Taruna PoltekSSN CA`<br/>
`ca.dn=CN=Taruna PoltekSSN CA, O=PoltekSSN, C=ID`<br/>
`ca.keyspec=2048`<br/>
`ca.validity=3650`<br/>

#### d. Konfigurasi properties web EJBCA

`vi web.properties`

Setting properties web berikut ini:

`httpsserver.dn=CN=${httpsserver.hostname}, O=PoltekSSN, C=ID`<br/>

### Deployment EJBCA ke Wildfly

Masuk ke directory `/opt`

`sudo chown -R wildfly: ejbca_ce_7_4_3_2`

Masuk ke directory `/opt/ejbca_ce_7_4_3_2`

`sudo -u wildfly ant -q clean deployear` <br/>
`sudo -u wildfly ant -q runinstall`<br/>
`sudo -u wildfly ant -q deploy-keystore`<br/>

### Finalisasi dan Pengujian Instalasi EJBCA

Copy superadmin.p12 ke directory `/tmp`. Masuk ke directory `/opt/ejbca_ce_7_4_3_2/p12`

`cp superadmin.p12 /tmp`<br/>
`chmod 755 superadmin.p12` pada direktori `/tmp`<br/>

Copy file superadmin.p12 ke komputer lokal Anda.

Akses <IP VM>:8443/ejbca/adminweb/. Anda akan diminta untuk memasukkan filer sertifikat. Gunakan file sertifikat superadmin.p12 untuk masuk ke halaman Administrator EJBCA

Apabila Anda berhasil masuk ke halaman Administrator EJBCA, artinya proses instalasi telah berhasil dilakukan.
